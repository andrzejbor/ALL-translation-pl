﻿# ALL-translation-pl
Polish translation of "Adaptive LL(*) Parsing" by Parr,Harwell and Fisher

Adaptive LL(*) Parsing: The Power of Dynamic Analysis
Terence Parr,Sam Harwell,Kathleen Fisher
http://www.antlr.org/papers/allstar-techreport.pdf

Wytyczne:
- edytor TeXworks.
- konwersja pdfLatex
- UTF8 ma BOM
- podział na krókie wiersze dla porównań gita